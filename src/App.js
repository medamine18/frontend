import { Component} from 'react';
import './App.css';
import SideBar from './componets/SideBar/SideBar';
import Workspace from './componets/Workspace/Workspace';

class App extends Component {

  state = {
    pickedTemplate: null,
  }


  clickHandler = type => {
    switch(type) {
      case 'Choisir une autre template':
          this.setState({
            pickedTemplate: null,
          });
          break;
      case 'Telecharger ce modèle':
          window.print();
          break;
      default:
          console.error('Something went wrong');
    }
  }


  render() {
    
    return (
      <div className="App">
        <SideBar 
          {...this.state}
          clickHandler={this.clickHandler}/>
        <Workspace 
          state={this.state}
          onSetArtboard={picked => this.setState({pickedTemplate: picked}) }
          />
      </div>
    );
  }
}

export default App;
