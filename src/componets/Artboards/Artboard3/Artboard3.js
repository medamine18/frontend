import generalClasses from "../Artboard.module.css";
import classes from "./classes.module.css";
import React, { useEffect,useState } from 'react';
import axios from "axios";

const Artboard3 = (props) => {


    const [CandidateData, setCandidateData] = useState([{}]);
    const [ExperienceData, setExperienceData] = useState([{}]);

    useEffect( () => {
            const Candidate = async () => {

                try {
                     const response = await axios.get("http://localhost:5000");
                    setCandidateData(response.data);
                    console.log(response.data);
                 } catch (error) {
                    console.log(error);
                }
            }
            const Experience = async () => {

                try {
                    const response = await axios.get("http://localhost:5000/experience");
                    setExperienceData(response.data);
                    console.log(response.data);
                } catch (error) {
                    console.log(error);
                }
            }
            Candidate();
            Experience();
        },
        []);


    const fetchStringData = data => {
        if(!data) return "";
        else {
            const res = JSON.stringify(data);
            return res.substring(1,res.length-1);
        }

    }

    const fetchAddress = address => {
        if(!address) return "";
        else {
            const address1 = JSON.stringify(address.address1);
            const nn = address1.substring(1,address1.length-1);
            const city = JSON.stringify(address.city);
            const cc = city.substring(1,city.length-1);
            const country = JSON.stringify(address.countryID);
            const kk = country.substring(1,country.length-1);
            return nn+" - "+ cc+" - "+kk;
        }

    }

    const fetchArrayData = (arrayData) => {
        if(!arrayData) return [];
        else {
            return (
                <ul className={classes.list}>
                    {
                        arrayData.map(item => {
                            return <li key={item} className={classes.items}>{item} </li>;
                        })}
                </ul>
            );
        }

    }


    const fetchExperienceSummaryData = data => {
        if(!data) return "";
        else {
            const res = JSON.stringify(data);
            const summary = res.substring(1,res.length-1);
            console.log(summary);

            const samara= summary.replace(/\\n/g, " ").replace(/> /g, " ");
            console.log(samara);

            const usingSplit = samara.split('  ');
            console.log(usingSplit);

            return (
                <ul className={classes.summary}>
                    {
                        usingSplit.map(item => {
                            return <li key={item} className={classes.summary_items}>{item} </li>;
                        })}
                </ul>
            );

    }
        }


    const fetchExperienceDate = date => {
        if(!date) return "";
        else {
            const unixTimestamp  = JSON.stringify(date);
            console.log(unixTimestamp);

           /* const milliseconds = unixTimestamp*1000;
            console.log(milliseconds);*/

            const startDate = new Date(unixTimestamp*1);
            console.log(startDate);

            const humanDateFormat = startDate.toLocaleString()
            console.log(humanDateFormat);

            let startMonth= "" ;
            switch (startDate.getMonth()) {
                case 0:
                    startMonth = "janvier";
                    break;
                case 1:
                    startMonth = "Février";
                    break;
                case 2:
                    startMonth = "Mars";
                    break;
                case 3:
                    startMonth = "Avril";
                    break;
                case 4:
                    startMonth = "Mai";
                    break;
                case 5:
                    startMonth = "Juin";
                    break;
                case 6:
                    startMonth = "Juillet";
                    break;
                case 7:
                    startMonth = "Aout";
                    break;
                case 8:
                    startMonth = "Septembre";
                    break;
                case 9:
                    startMonth = "Octobre";
                    break;
                case 10:
                    startMonth = "November";
                    break;
                case 11:
                    startMonth = "Décembre";
                    break;
                default:
                    startMonth = "";

            }
            console.log(startMonth);

            const startYear = startDate.getFullYear();
            console.log(startYear);

            return startMonth +' '+startYear;
        }

    }

    /*axios
        .post('http://localhost:5000/resume')
        .then(() => console.log('page sent'))
        .catch(err => {
            console.error(err);
        });*/



    return (
        <div
            id="section-to-print"
            onClick={props.onClickArtboard}
            className={[generalClasses.container, classes.container].join(" ")}
            style={{ transform: `scale(${props.zoom / 100})`, ...props.style }}
        >


            <img src={'assets/user.png'} className={classes.pic} alt="Club Freelance" />

            <div id="contact-info" className={classes.info}>

                <h1 className="fn">{fetchStringData(CandidateData[0].name)}</h1>
                <p>
                    Phone: <span className="tel">{fetchStringData(CandidateData[0].phone)}</span>
                    <br />
                    Email: <span className="email">{fetchStringData(CandidateData[0].email)}</span>
                    <br />
                    Gender: <span className="email">{fetchStringData(CandidateData[0].gender)}</span>
                    <br />
                    Address: <span className="email">{fetchAddress(CandidateData[0].address)}</span>
                </p>
            </div>
            <div className={classes.objective}>
                <p>{fetchStringData(CandidateData[0].occupation)}</p>
            </div>
            <dl>
                <dd className={classes.clear}></dd>

                <dt className={classes.experiences_container}><img src={'assets/user.png'} className={classes.logo} alt="Club Freelance" />Expériences</dt>
                <dd>
                    <h2>
                        {fetchStringData(ExperienceData[0].companyName)}
                        <span>
              {fetchStringData(ExperienceData[0].title)}, {fetchExperienceDate(ExperienceData[0].startDate)}-
              {fetchExperienceDate(ExperienceData[0].endDate)}
            </span>
                    </h2>
                    <p>{fetchExperienceSummaryData(ExperienceData[0].summary)}</p>
                </dd>

                <dd className={classes.clear}></dd>

                <dt className={classes.skills_container}><img src={'assets/user.png'} className={classes.logo} alt="Club Freelance" />Skills</dt>
                <dd className={classes.skills}>
                    <p>{fetchArrayData(CandidateData[0].skills)}</p>
                </dd>



                <dd className={classes.clear}></dd>

                <dt className={classes.categories_container}><img src={'assets/user.png'} className={classes.logo} alt="Club Freelance" />Catégories</dt>
                <dd>
                    <p>{fetchArrayData(CandidateData[0].categories)}</p>
                </dd>

                <dd className={classes.clear}></dd>

                <dt className={classes.specialities_container}><img src={'assets/user.png'} className={classes.logo} alt="Club Freelance" />Spécialities</dt>
                <dd>
                    <p>{fetchArrayData(CandidateData[0].specialties)}</p>
                </dd>

                <dd className={classes.clear}></dd>

                <dt className={classes.langues_container}><img src={'assets/user.png'} className={classes.logo} alt="Club Freelance" />Langues</dt>
                <dd>
                    <p>{fetchArrayData(CandidateData[0].spokenLanguages)}</p>
                </dd>

                <dd className={classes.clear}></dd>
            </dl>

            <dd className={classes.clear}></dd>
        </div>
    );
};

export default Artboard3;
