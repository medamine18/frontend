import React, { Component } from "react";
import axios from "../../axios-orders";

class Experience extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ExperienceData: []
        };
    }

    componentDidMount() {
        axios
            .get("https://localhost:5000")
            .then((response) => {
                this.setState({ ExperienceData: response.data });
            })
            .catch((error) => {
                this.setState({ error: true });
            });
    }
}

export default Experience;
