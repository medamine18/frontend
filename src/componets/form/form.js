import React from 'react';
import { useState } from "react";
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function CandidatID(props) {

    const classes = useStyles();
    const [ID, setID] = useState('');

    const addID = event => {
        if(event.key === 'Enter') {
            setID(ID);
        }
    }
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar alt="Club Freelance" src="../assets/logos/LogoClubFreelance.png" />
                <Typography component="h1" variant="h5">
                    Veuillez ajouter l'ID du candidat
                </Typography>
                <form className={classes.form} noValidate>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="ID"
                        name="ID"
                        autoComplete="ID"
                        autoFocus
                        label="Candidate ID"
                        icon="user"
                        group type="text"
                        validate error="wrong"
                        success="right"
                        type="text"
                        value={setID}
                        onKeyPress={addID}
                        onChange={e =>setID(e.target.value)}
                    />

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={() => props.onProgress(ID)}
                    >
                        Envoyer
                    </Button>
                </form>
            </div>
        </Container>
    );
}